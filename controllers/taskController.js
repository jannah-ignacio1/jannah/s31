//Controller contain the functions and business logic of our Express JS app

const Task = require("../models/task");

//Controller function for getting all the tasks
//Defines the functions to be used in the "taskRoute.js" file and exports these functions
module.exports.getAllTasks = () => {

	//model method
	return Task.find({}).then(result => {
		return result;
	})
}

//Controller function for creating task
//gets data
//The requestBody coming from the client was passed from the "taskRoute.js" file via the "req.body" as an argument and is renamed as a "requestBody" parameter in the controller file
module.exports.createTask = (requestBody) => {

	let newTask = new Task({
		name:requestBody.name
	})
	//saves the newly created "newTask" object in our MongoDB database
	//The 'then' methods waits until the task is tored in the database or an error is encountered before returning a "true" or "false" value back to the client 
	//then will accept task or error (2 arguments)
		//first parameter will store the result returne by the Mongoose "save" method
		//second parameter will store the 'error' object
	return newTask.save().then((task, error) =>{
		if (error){
			console.log(error)
			return false
		} else {
			return task
		}
		})
}

//Controller for deleting a Task
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,err) =>{
		if(err){
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}

//Controller function for updating a Task
module.exports.updateTask = (taskId, newContent) => {
		return Task.findById(taskId).then((result, err) =>{
			if (err) {
				console.log(err)
				return false
			}
			//reassigning from the findId to newContent(req.body)
			result.name = newContent.name;

			return result.save().then((updatedTask,saveErr)=>{
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
		})
}

//Controller function for getting and updating status Task
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.updateTask = (taskId, newContent) => {
		return Task.findById(taskId).then((result, err) =>{
			if (err) {
				console.log(err)
				return false
			}
			result.status = newContent.status;

			return result.save().then((updatedTask,saveErr)=>{
				if(saveErr){
					console.log(saveErr)
					return false
				} else {
					return updatedTask
				}
			})
		})
}