//Contains all of the endpoints of our application

//We need to use express' Router() function to achieve this 
const express = require("express")

//Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//The "taskController" allows us to use the functions defined in the taskController.js file
const taskController = require("../controllers/taskController")

//Route to get all the tasks
router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultFromController =>
		res.send(resultFromController));
});

//Route to create a task
//This route expects to receive POST request at the URL "/tasks/"
router.post("/", (req, res)=>{
	console.log(req.body)
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController))
})

//Route for deleting a Task
router.delete("/:id", (req,res)=> {
	console.log(req.params)
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(`Task ${resultFromController.name} is deleted`));
}); //http://localhost:4000/tasks/621e0bb91c453c4c272d167b


//Route to update a Task
router.put("/:id", (req, res)=>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController))
})


//ROUTE TO GET AND UPDATE STATUS TASK
router.get("/:id", (req, res) =>{
	taskController.getAllTasks().then(resultFromController =>
		res.send(resultFromController));
});

router.put("/:id/complete", (req, res)=>{
	console.log(req.params)
	taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController))
})



//Use "module.expports" to export the router object to use in the index.js
module.exports = router;
